#include "kc_reader.h"
#include "ui_kc_reader.h"

KC_Reader::KC_Reader(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::KC_Reader)
{
    ui->setupUi(this);
}

KC_Reader::~KC_Reader()
{
    delete ui;
}

void KC_Reader::on_pushButton_cancel_clicked()
{
    QApplication::quit();
}

void KC_Reader::on_pushButton_login_clicked()
{
    if(ui->lineEdit_username->text() == "username" && ui->lineEdit_password->text() == "password")
        {
            this->hide();
            out_dia = new Output_Dia(this);
            out_dia->show();
            out_dia->show_fighters();

        }
        else
        {
            ui->statusBar->showMessage("Wrong username &/or password", 5000);
        }
}
