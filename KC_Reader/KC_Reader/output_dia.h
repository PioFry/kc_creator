#ifndef OUTPUT_DIA_H
#define OUTPUT_DIA_H

#include <QDialog>
#include <QLabel>
#include <QCheckBox>
#include "diagram.h"

namespace Ui {
class Output_Dia;
}

class Output_Dia : public QDialog
{
    Q_OBJECT

public:
    explicit Output_Dia(QWidget *parent = nullptr);
    ~Output_Dia();

    void show_fighters();
private slots:
    void on_pushButton_cancel_clicked();

    void on_pushButton_diagram_clicked();

private:
    Ui::Output_Dia *ui;
    Diagram *diag;
    std::vector<QLabel*> labels;
    std::vector<QCheckBox*> checks;
    void read_labels();
    void create_checkboxes();
};

#endif // OUTPUT_DIA_H
