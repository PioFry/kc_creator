#include "output_dia.h"
#include "ui_output_dia.h"
#include <QBoxLayout>
#include <QFormLayout>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>


Output_Dia::Output_Dia(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Output_Dia)
{
    for(QLabel* l : labels)
    {
        delete l;
    }
    for(QCheckBox* cb : checks)
    {
        delete cb;
    }
    ui->setupUi(this);
}

Output_Dia::~Output_Dia()
{
    delete ui;
}

void Output_Dia::read_labels()
{
    QFile file(":/txt/txt_files/saving_file.txt");

    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Error", "Couldn't open file");
    }
    QTextStream in(&file);
    QString line;
    while(!in.atEnd())
    {
        in.readLineInto(&line);
        labels.push_back(new QLabel(line));
    }
}

void Output_Dia::create_checkboxes()
{
    for(QLabel* l : labels)
    {
        checks.push_back(new QCheckBox);
    }
}

void Output_Dia::show_fighters()
{
    read_labels();
    create_checkboxes();
    QFormLayout *layout = new QFormLayout;
    QWidget *fout = new QWidget;
    for(size_t i = 0; i < labels.size(); i++)
    {
        layout->addWidget(labels[i]);
        layout->addWidget(checks[i]);
    }

    fout->setLayout(layout);
    ui->scrollArea->setWidget(fout);
    ui->scrollArea->setWidgetResizable(true);
}

void Output_Dia::on_pushButton_cancel_clicked()
{
    QApplication::quit();
}

void Output_Dia::on_pushButton_diagram_clicked()
{
    this->hide();
    diag = new Diagram(this);
    diag->show();
    diag->present_fighters(labels, checks);
}
