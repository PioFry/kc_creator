#ifndef DIAGRAM_H
#define DIAGRAM_H

#include <QDialog>
#include <QLabel>
#include <QCheckBox>

namespace Ui {
class Diagram;
}

class Diagram : public QDialog
{
    Q_OBJECT

public:
    explicit Diagram(QWidget *parent = nullptr);
    ~Diagram();

    void present_fighters(const std::vector<QLabel*>& labels, const std::vector<QCheckBox*>& checks);
private slots:
    void on_pushButton_close_clicked();

private:
    Ui::Diagram *ui;
};

#endif // DIAGRAM_H
