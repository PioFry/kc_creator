#include "diagram.h"
#include "ui_diagram.h"
#include <QBoxLayout>
#include <QFormLayout>

Diagram::Diagram(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Diagram)
{
    ui->setupUi(this);
}

Diagram::~Diagram()
{
    delete ui;
}

void Diagram::present_fighters(const std::vector<QLabel *> &labels, const std::vector<QCheckBox *> &checks)
{
    QFormLayout *layout = new QFormLayout;
    QWidget *fout = new QWidget;
    for(size_t i = 0; i < checks.size(); i++)
    {
        if(checks[i]->isChecked())
        {
            layout->addWidget(labels[i]);
        }
    }
    fout->setLayout(layout);
    ui->scrollArea->setWidget(fout);
    ui->scrollArea->setWidgetResizable(true);
}

void Diagram::on_pushButton_close_clicked()
{
    QApplication::quit();
}
