#ifndef KC_READER_H
#define KC_READER_H

#include <QMainWindow>
#include "output_dia.h"

namespace Ui {
class KC_Reader;
}

class KC_Reader : public QMainWindow
{
    Q_OBJECT

public:
    explicit KC_Reader(QWidget *parent = nullptr);
    ~KC_Reader();

private slots:
    void on_pushButton_cancel_clicked();

    void on_pushButton_login_clicked();

private:
    Ui::KC_Reader *ui;
    Output_Dia *out_dia;

};

#endif // KC_READER_H
