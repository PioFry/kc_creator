/********************************************************************************
** Form generated from reading UI file 'output_dia.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OUTPUT_DIA_H
#define UI_OUTPUT_DIA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Output_Dia
{
public:
    QVBoxLayout *verticalLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_diagram;

    void setupUi(QDialog *Output_Dia)
    {
        if (Output_Dia->objectName().isEmpty())
            Output_Dia->setObjectName(QString::fromUtf8("Output_Dia"));
        Output_Dia->resize(581, 340);
        verticalLayout = new QVBoxLayout(Output_Dia);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        scrollArea = new QScrollArea(Output_Dia);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 561, 289));
        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout->addWidget(scrollArea);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButton_cancel = new QPushButton(Output_Dia);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));

        horizontalLayout->addWidget(pushButton_cancel);

        horizontalSpacer = new QSpacerItem(300, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_diagram = new QPushButton(Output_Dia);
        pushButton_diagram->setObjectName(QString::fromUtf8("pushButton_diagram"));

        horizontalLayout->addWidget(pushButton_diagram);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(Output_Dia);

        QMetaObject::connectSlotsByName(Output_Dia);
    } // setupUi

    void retranslateUi(QDialog *Output_Dia)
    {
        Output_Dia->setWindowTitle(QApplication::translate("Output_Dia", "Dialog", nullptr));
        pushButton_cancel->setText(QApplication::translate("Output_Dia", "Cancel", nullptr));
        pushButton_diagram->setText(QApplication::translate("Output_Dia", "Make Diagram", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Output_Dia: public Ui_Output_Dia {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OUTPUT_DIA_H
