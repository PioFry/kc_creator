/********************************************************************************
** Form generated from reading UI file 'kc_reader.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KC_READER_H
#define UI_KC_READER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_KC_Reader
{
public:
    QWidget *centralWidget;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QLineEdit *lineEdit_username;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *lineEdit_password;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_login;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *KC_Reader)
    {
        if (KC_Reader->objectName().isEmpty())
            KC_Reader->setObjectName(QString::fromUtf8("KC_Reader"));
        KC_Reader->resize(669, 348);
        centralWidget = new QWidget(KC_Reader);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        widget = new QWidget(centralWidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(270, 80, 266, 111));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(15, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        lineEdit_username = new QLineEdit(widget);
        lineEdit_username->setObjectName(QString::fromUtf8("lineEdit_username"));

        horizontalLayout->addWidget(lineEdit_username);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        lineEdit_password = new QLineEdit(widget);
        lineEdit_password->setObjectName(QString::fromUtf8("lineEdit_password"));
        lineEdit_password->setFocusPolicy(Qt::ClickFocus);
        lineEdit_password->setEchoMode(QLineEdit::Password);
        lineEdit_password->setReadOnly(false);

        horizontalLayout_2->addWidget(lineEdit_password);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        pushButton_cancel = new QPushButton(widget);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));

        horizontalLayout_3->addWidget(pushButton_cancel);

        horizontalSpacer = new QSpacerItem(100, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        pushButton_login = new QPushButton(widget);
        pushButton_login->setObjectName(QString::fromUtf8("pushButton_login"));

        horizontalLayout_3->addWidget(pushButton_login);


        verticalLayout->addLayout(horizontalLayout_3);

        KC_Reader->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(KC_Reader);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 669, 21));
        KC_Reader->setMenuBar(menuBar);
        mainToolBar = new QToolBar(KC_Reader);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        KC_Reader->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(KC_Reader);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        KC_Reader->setStatusBar(statusBar);

        retranslateUi(KC_Reader);

        QMetaObject::connectSlotsByName(KC_Reader);
    } // setupUi

    void retranslateUi(QMainWindow *KC_Reader)
    {
        KC_Reader->setWindowTitle(QApplication::translate("KC_Reader", "KC_Reader", nullptr));
        label->setText(QApplication::translate("KC_Reader", "Login", nullptr));
        label_2->setText(QApplication::translate("KC_Reader", "Password", nullptr));
        pushButton_cancel->setText(QApplication::translate("KC_Reader", "Cancel", nullptr));
        pushButton_login->setText(QApplication::translate("KC_Reader", "Login", nullptr));
    } // retranslateUi

};

namespace Ui {
    class KC_Reader: public Ui_KC_Reader {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KC_READER_H
