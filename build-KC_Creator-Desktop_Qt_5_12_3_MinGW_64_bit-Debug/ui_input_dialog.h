/********************************************************************************
** Form generated from reading UI file 'input_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INPUT_DIALOG_H
#define UI_INPUT_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Input_Dialog
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton_add;
    QHBoxLayout *horizontalLayout_10;
    QPushButton *pushButton_cancel;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_done;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;

    void setupUi(QDialog *Input_Dialog)
    {
        if (Input_Dialog->objectName().isEmpty())
            Input_Dialog->setObjectName(QString::fromUtf8("Input_Dialog"));
        Input_Dialog->resize(672, 362);
        Input_Dialog->setLayoutDirection(Qt::LeftToRight);
        layoutWidget = new QWidget(Input_Dialog);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(20, 300, 621, 56));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton_add = new QPushButton(layoutWidget);
        pushButton_add->setObjectName(QString::fromUtf8("pushButton_add"));

        verticalLayout->addWidget(pushButton_add);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        pushButton_cancel = new QPushButton(layoutWidget);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));

        horizontalLayout_10->addWidget(pushButton_cancel);

        horizontalSpacer = new QSpacerItem(400, 0, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer);

        pushButton_done = new QPushButton(layoutWidget);
        pushButton_done->setObjectName(QString::fromUtf8("pushButton_done"));

        horizontalLayout_10->addWidget(pushButton_done);


        verticalLayout->addLayout(horizontalLayout_10);

        scrollArea = new QScrollArea(Input_Dialog);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setGeometry(QRect(20, 10, 621, 281));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 619, 279));
        scrollArea->setWidget(scrollAreaWidgetContents);

        retranslateUi(Input_Dialog);

        QMetaObject::connectSlotsByName(Input_Dialog);
    } // setupUi

    void retranslateUi(QDialog *Input_Dialog)
    {
        Input_Dialog->setWindowTitle(QApplication::translate("Input_Dialog", "Contestants", nullptr));
        pushButton_add->setText(QApplication::translate("Input_Dialog", "Add new fighter", nullptr));
        pushButton_cancel->setText(QApplication::translate("Input_Dialog", "Cancel", nullptr));
        pushButton_done->setText(QApplication::translate("Input_Dialog", "Done", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Input_Dialog: public Ui_Input_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INPUT_DIALOG_H
