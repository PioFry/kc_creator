#include "input_dialog.h"
#include "ui_input_dialog.h"
#include <iostream>
#include <fstream>
#include <QString>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QValidator>

void create_labels(std::vector<QLabel*> labels);

Input_Dialog::Input_Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Input_Dialog)
{
    ui->setupUi(this);
}

Input_Dialog::~Input_Dialog()
{
    for(QLineEdit* line : lines)
    {
        delete line;
    }
    for(QLabel* label : labels)
    {
        delete label;
    }
    delete ui;
}



void Input_Dialog::on_pushButton_done_clicked()
{
    for(size_t j = 1; j <= lines.size(); j++)
    {
        if(lines[j-1]->text() == "")
        {
            QMessageBox::warning(this, "Error", "You left blank lines!");
            return;
        }
    }
    QFile file("D:/KC_Reader/KC_Reader/txt_files/saving_file.txt");

    if(!file.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Error", "During opening the file, an error occured");
    }
    QTextStream out(&file);
    for(size_t i = 1; i <= lines.size(); i++)    //We start iteration from 1 because we want i % 4 to work
    {
        out << lines[i-1]->text() << " ";
        if(i % 4 == 0)
        {
            out << "\n";
        } 
    }
    file.flush();
    file.close();
    std::cout << "Done writting to file" << std::endl;
    QMessageBox::information(this, "Succes", "Your application has been saved!");
    QApplication::exit();

}

void Input_Dialog::on_pushButton_add_clicked()
{
    //Qt is supposed to take care of all these pointers, otherwise should they should be deleted (check ~Input_Dialog())
    QValidator *ageval = new QIntValidator(0, 100, this);
    QValidator *weightval = new QDoubleValidator(0.0, 300.0, 2, this);
    QRegExp alpha("^[A-Za-z]+$");
    QValidator *namesval = new QRegExpValidator(alpha, this);
    QLineEdit *lname = new QLineEdit;
    QLineEdit *lsurname = new QLineEdit;
    QLineEdit *lage = new QLineEdit;
    QLineEdit *lweight = new QLineEdit;
    lname->setValidator(namesval);
    lsurname->setValidator(namesval);
    lage->setValidator(ageval);
    lweight->setValidator(weightval);
    lines.push_back(lname);
    lines.push_back(lsurname);
    lines.push_back(lage);
    lines.push_back(lweight);
    QLabel *n = new QLabel("name");
    QLabel *s = new QLabel("surname");
    QLabel *a = new QLabel("age");
    QLabel *w = new QLabel("weight");
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(n);
    layout->addWidget(lname);
    layout->addWidget(s);
    layout->addWidget(lsurname);
    layout->addWidget(a);
    layout->addWidget(lage);
    layout->addWidget(w);
    layout->addWidget(lweight);
    QWidget *fin = new QWidget;
    fin->setLayout(layout);
    Input_Dialog::scrbarlayout->addWidget(fin);
    QWidget *res = new QWidget;
    res->setLayout(scrbarlayout);
    ui->scrollArea->setWidget(res);
    ui->scrollArea->setWidgetResizable(true);
}

void Input_Dialog::on_pushButton_cancel_clicked()
{
    QApplication::quit();
}
