#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_cancel_clicked()
{
    QApplication::quit();
}

void MainWindow::on_pushButton_login_clicked()
{
    if(ui->lineEdit_username->text() == "username" && ui->lineEdit_password->text() == "password")
    {
        this->hide();
        input_dia = new Input_Dialog(this);
        input_dia->show();
    }
    else
    {
        ui->statusBar->showMessage("Wrong username &/or password", 5000);
    }
}
