#ifndef INPUT_DIALOG_H
#define INPUT_DIALOG_H

#include <QDialog>
#include <QLineEdit>
#include <QLabel>
#include <QVBoxLayout>


namespace Ui {
class Input_Dialog;
}

class Input_Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Input_Dialog(QWidget *parent = nullptr);
    ~Input_Dialog();

private slots:

    void on_pushButton_done_clicked();

    void on_pushButton_add_clicked();

    void on_pushButton_cancel_clicked();

private:
    Ui::Input_Dialog *ui;
    std::vector<QLineEdit*> lines;  //1rst - name, 2nd - surname, 3rd - age, 4th - weight <- loops every 4 QLineEdit's
    std::vector<QLabel*> labels;    //Used for easier adding and delete
    QVBoxLayout *scrbarlayout = new QVBoxLayout;
};

#endif // INPUT_DIALOG_H
